const Express = require('express');
const App = Express();
const database = require('../database');
const db = database.db('school').collection('classes');
const dbcourse = database.db('school').collection('courses');

const classes = [
    {
        classID: 101,
        courses: [
            [{ course: 0 }, { course: 0 }, { course: 0 }, { course: 0 },],
            [{ course: 3 }, { course: 3 }, { course: 3 }, { course: 3 },],
            [{ course: 2 }, { course: 2 }, { course: 2 }, { course: 2 },],
            [{ course: 1 }, { course: 1 }, { course: 1 }, { course: 1 },],
            [{ course: 4 }, { course: 4 }, { course: 4 }, { course: 4 },]
        ]
    },
    {
        classID: 102,
        courses: [
            [{ course: 1 }, { course: 1 }, { course: 1 }, { course: 1 },],
            [{ course: 3 }, { course: 3 }, { course: 3 }, { course: 3 },],
            [{ course: 0 }, { course: 0 }, { course: 0 }, { course: 0 },],
            [{ course: 1 }, { course: 2 }, { course: 3 }, { course: 0 },],
            [{ course: 4 }, { course: 4 }, { course: 4 }, { course: 4 },]
        ]
    },
    {
        classID: 103,
        courses: [
            [{ course: 2 }, { course: 2 }, { course: 2 }, { course: 2 },],
            [{ course: 0 }, { course: 0 }, { course: 0 }, { course: 0 },],
            [{ course: 1 }, { course: 1 }, { course: 1 }, { course: 1 },],
            [{ course: 3 }, { course: 3 }, { course: 3 }, { course: 3 },],
            [{ course: 4 }, { course: 4 }, { course: 4 }, { course: 4 },]
        ]
    },
];

App.post('/', async (req, res, next) => {
    var courses = [];
    await dbcourse.find({}).forEach((r) => {
        courses.push(r);
    });

    classes.forEach((a) => {
        a.courses.forEach((b) => {
            b.forEach((c) => {
                c.course = courses[c.course];
            });
        });
    })

    await db.insertMany(classes);
    res.status(200).json({ result: classes });
});
App.delete('/', (req, res, next) => {
    db.drop();
    res.status(201).json({ result: 'deleted' });
})

module.exports = App;