const app = require('express')();
const database = require('../database').db('school');
const dbstudent = database.collection('unRegisterUsers');
const dbMessage = database.collection('message');
const oi = require('mongodb').ObjectID;

app.post('/', async (req, res) => {
    const message = req.body.message;
    const subject = req.body.subject;
    const from = req.body.from;
    const time = req.body.time;
    const to = req.body.to;
    var messageObject = { message, subject, from, to, time }

    // await dbMessage.insertOne(messageObject);

    if (to) {
        const x = await dbstudent.findOne({ _id: oi(to) })
        if (!x) return res.status(404).json({ result: 'not found' })
        let messages = x['message']
        await messages.push(messageObject);
        await dbstudent.updateOne({ _id: oi(to) }, { $set: { message: messages } })
    } else {
        // await dbstudent.updateMany({}, { $set: { scores: [], abpr: [], behavior: [], message: [] } })
        await dbstudent.find({}).forEach(async (f) => {
            var messages = []
            f.message.forEach((x) => {
                messages.push(x);
            })
            messages.push(messageObject);
            await dbstudent.updateOne({ _id: f._id }, { $set: { message: messages } });
        });
    }

    res.status(200).json({ result: messageObject })
})

module.exports = app;