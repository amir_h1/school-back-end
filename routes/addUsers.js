const Express = require('express');
const App = Express();
const ObjectID = require('mongodb').ObjectID;
const database = require('../database');
const dbReg = database.db('school').collection('unRegisterUsers');
const dbregUsers = database.db('school').collection('regUsers');

const users = [
    {
        _id: new ObjectID(),
        username: 'amir',
        family: 'javadi',
        idNumber: 1000,
        isRegister: false,
        password: null,
        phonenumber: 91212345678,
        authlvl: 1,
        class: 101,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    }, {
        _id: new ObjectID(),
        username: 'mahdi',
        family: 'irani',
        idNumber: 1001,
        isRegister: false,
        password: null,
        phonenumber: 91212345672,
        class: 102,
        authlvl: 1,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    }, {
        _id: new ObjectID(),
        username: 'iman',
        family: 'mohseni',
        idNumber: 1002,
        isRegister: false,
        password: null,
        phonenumber: 91212345673,
        class: 101,
        authlvl: 1,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    }, {
        _id: new ObjectID(),
        username: 'ali',
        family: 'mehrabi',
        idNumber: 1003,
        isRegister: false,
        password: null,
        phonenumber: 91212345674,
        class: 103,
        authlvl: 1,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    }, {
        _id: new ObjectID(),
        username: 'amin',
        family: 'majafi',
        idNumber: 1004,
        isRegister: false,
        password: null,
        phonenumber: 91212345675,
        class: 102,
        authlvl: 1,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    }, {
        _id: new ObjectID(),
        username: 'javad',
        family: 'amir',
        idNumber: 1005,
        isRegister: false,
        password: null,
        phonenumber: 91212345676,
        class: 101,
        authlvl: 1,
        abpr: [],
        scores: [],
        behavior: [],
        message: []
    },
];
App.post('/', async (req, res, next) => {
    await dbReg.insertMany(users);
    res.status(200).json({ result: 'users added to db' });
});

App.delete('/', async (req, res, next) => {
    var a = [];
    await dbReg.find({}).forEach((r) => {
        a.push(r);
    })
    var b = [];
    await dbregUsers.find({}).forEach((r) => {
        b.push(r);
    })

    if (a.length > 0) await dbReg.drop();
    if (b.length > 0) await dbregUsers.drop();
    res.status(200).json({ result: 'all student deleted' });
});

module.exports = App;