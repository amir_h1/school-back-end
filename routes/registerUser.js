const Express = require('express');
const App = Express();
const db = require('../database');
const dbReg = db.db('school').collection('unRegisterUsers');

App.get('/', async (req, res, next) => {
    var items = [];
    await dbReg.find({}).forEach((student) => {
        items.push({ name: student.username, class: student.class, });
    })
    res.status(200).json({ result: items });
});

App.put('/', async (req, res, next) => {
    const student = {
        idNumber: req.body.idNumber,
        password: req.body.password,
    }
    console.log(student);
    
    var find = await dbReg.findOne({ idNumber: student.idNumber });
    if (!find) {
        return res.status(404).json({ result: 'invalid userId' })
    } else if (find.isRegister == false) {
        await dbReg.updateOne(find, { $set: { password: student.password, isRegister: true } });
        res.status(201).json({ result: 'user registerd' })
    } else {
        res.status(500).json({ result: 'you registerd before' })
    }

});

module.exports = App;