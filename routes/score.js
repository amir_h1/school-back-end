const Express = require('express');
const database = require('../database').db('school');
const dbstudent = database.collection('unRegisterUsers');
const dbcourse = database.collection('courses');
const app = Express();
const OI = require('mongodb').ObjectID;

app.post('/', async (req, res) => {
    const scores = req.body.list;
    const classID = req.body.class;

    // await dbstudent.updateMany({}, { $set: { scores: [], abpr: [], behavior: [], message: [] } })

    if (!scores) return res.status(500).json({ result: 'no list' });
    if (!classID) return res.status(500).json({ result: 'no class' });

    var stList = [];
    await dbstudent
        .find({ class: classID })
        .forEach((f) => {
            stList.push(f)
        })

    stList.forEach(async (f, index) => {
        const id = OI(f._id).toString();
        const filter = await scores.filter(x => x._id === id)

        if (filter[0]) {
            await stList[index].scores
                .push(filter[0]);
            await dbstudent
                .findOneAndUpdate({ _id: stList[index]._id }, { $set: { scores: stList[index].scores } })
        }
    })

    res.status(201).json({ result: 'added' })
})

module.exports = app;