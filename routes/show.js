const Express = require('express');
const database = require('../database').db('school');
const dbstudent = database.collection('unRegisterUsers');
const dbcourse = database.collection('courses');
const dbpresonnel = database.collection('presonnel');
const app = Express();
const oi = require('mongodb').ObjectID;

app.post('/', async (req, res, next) => {
    const classID = { class: req.body.classID };
    const teacherID = { teacherID: req.body.teacherID };
    if (!classID.class) return res.status(500).json({ result: 'class id miss' });
    if (!teacherID.teacherID) return res.status(500).json({ result: 'teacher id miss' });

    var list = [];
    await dbstudent.find(classID).forEach((r) => {
        list.push(r);
    });

    var courses = [];
    await dbcourse.find({}).forEach((f) => {
        const name = f.lacture;
        const teacherId = f.teacherID.ID;
        const teacherName = f.teacherID.name;
        const teacherFamily = f.teacherID.family;
        courses.push({ name, teacherId, teacherName, teacherFamily });
    });

    const find = courses.find(f => f.teacherId === teacherID.teacherID)
    if (!find) return res.status(404).json({ result: 'not found' });

    for (const item of list) {
        var a = item.abpr.filter(f => f.from === find.teacherId)
        a.forEach(f => {
            f.from = find
        })
        item.abpr = a;

        var b = item.scores.filter(f => f.from === find.teacherId)
        b.forEach(f => {
            f.from = find
        })
        item.scores = b;

        var c = item.behavior.filter(f => f.from === find.teacherId)
        c.forEach(f => {
            f.from = find
        })
        item.behavior = c;
    }

    res.status(200).json({ result: list });
});

app.post('/student', async (req, res) => {
    const user = req.body.user;
    const find = await dbstudent.findOne({ _id: oi(user) });
    if (!find) return res.status(404).json({ result: 'not found' })
    var presonnel = [];
    await dbpresonnel.find({}).forEach((f) => {
        const teacherId = f.ID;
        const teacherName = f.name;
        const teacherFamily = f.family;
        presonnel.push({ teacherId, teacherName, teacherFamily });
    });
    var courses = [];
    await dbcourse.find({}).forEach((f) => {
        const name = f.lacture;
        const teacherId = f.teacherID.ID;
        const teacherName = f.teacherID.name;
        const teacherFamily = f.teacherID.family;
        courses.push({ name, teacherId, teacherName, teacherFamily });
    });

    find.abpr.forEach(f => {
        const findd = courses.find(a => a.teacherId === f.from)
        if (findd) {
            f.from = findd
        }
    });
    find.scores.forEach(f => {
        const findd = courses.find(a => a.teacherId === f.from)
        if (findd) {
            f.from = findd
        }
    });
    find.behavior.forEach(f => {
        const findd = presonnel.find(a => a.teacherId === f.from)
        if (findd) {
            f.from = findd
        }
    });
    find.message.forEach(f => {
        const findd = presonnel.find(a => a.teacherId === f.from)
        if (findd) {
            f.from = findd
        }
    });

    res.status(200).json({ result: find })

});

app.get('/', async (req, res, next) => {
    var list = [];
    await dbstudent.find({}).forEach((r) => {
        list.push(r);
    });

    var courses = [];
    await dbcourse.find({}).forEach((f) => {
        const name = f.lacture;
        const teacherId = f.teacherID.ID;
        const teacherName = f.teacherID.name;
        const teacherFamily = f.teacherID.family;
        courses.push({ name, teacherId, teacherName, teacherFamily });
    });

    for (const item of list) {
        item.scores.forEach(f => {
            const find = courses.find(a => a.teacherId === f.from)
            f.from = find;
        })
    }

    res.status(200).json({ result: list });
});

module.exports = app;
