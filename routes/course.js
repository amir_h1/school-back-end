const Express = require('express');
const App = Express();
const db = require('../database');
const dbcourse = db.db('school').collection('courses');
const dbpersonnel = db.db('school').collection('presonnel');

const courses = [
    { code: 0, lacture: 'riazi', teacherID: 1003 },
    { code: 1, lacture: 'fizik', teacherID: 1004 },
    { code: 2, lacture: 'shimi', teacherID: 1005 },
    { code: 3, lacture: 'zist', teacherID: 1006 },
    { code: 4, lacture: 'arabi', teacherID: 1007 }
]
App.post('/', async (req, res, next) => {
    var teacher = [];
    await dbpersonnel.find({ authLvl: 3 }).forEach((a) => {
        teacher.push({ ID: a.ID, name: a.name, family: a.family })
    })
    courses.forEach((a) => {
        a.teacherID = teacher.find(f => f.ID === a.teacherID)
    })

    await dbcourse.insertMany(courses);
    res.status(201).json({ result: courses });
})
module.exports = App;