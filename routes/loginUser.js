const Express = require('express');
const App = Express();
const database = require('../database');
const db = database.db('school').collection('unRegisterUsers');

App.post('/', async (req, res, next) => {
    const idNumber = { idNumber: req.body.idNumber }
    const password = { password: req.body.password }
    const find = await db.findOne(idNumber);
    if (!find) return res.status(404).json({ result: `not found` });
    if (find.isRegister === false) return res.status(500).json({ result: `register first` })
    if (find.password !== password.password) return res.status(401).json({ result: `icorrect password` })
    res.status(200).json({
        result: {
            _id: find._id,
            name: find.username,
            family: find.family,
            class: find.class,
            phonenumber: find.phonenumber,
        }
    })
});

module.exports = App;