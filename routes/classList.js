const Express = require('express');
const database = require('../database').db('school');
const dbclass = database.collection('classes')
const dbstudent = database.collection('regUsers');

const app = Express();

app.get('/', async (req, res, next) => {
    var list = [];
    var students = [];

    await dbstudent.find({}).forEach((r) => {
        students.push({ class: r.class, name: r.username, family: r.family, score: r.scores });
    });

    await dbclass.find({}).forEach((r) => {
        list.push({
            class: r.classID,
            students: students.filter(std => std.class === r.classID)
        });
    });

    res.status(200).json({ result: list });
})
app.post('/', async (req, res, next) => {
    res.status(200).json({ result: 'list' });
})
module.exports = app;