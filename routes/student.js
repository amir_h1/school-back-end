const Express = require('express');
const App = Express();
const database = require('../database');
const OI = require('mongodb').ObjectID;
const db = database.db('school').collection('regUsers');
const dbUnReg = database.db('school').collection('unRegisterUsers');
const dbClasses = database.db('school').collection('classes');

const day = new Date().getDay();
const getDay = () => {
    switch (day) {
        case 0:
            return 1;
        case 1:
            return 2;
        case 2:
            return 3;
        case 3:
            return 4;
        case 6:
            return 0;
        default:
            return null;
    }
}
App.post('/', async (req, res, next) => {
    var classes = [];
    await dbClasses.find({}).forEach((r) => {
        classes.push(r);
    });

    const id = { _id: OI(req.body.id) }
    var find = await db.findOne(id);

    if (!find) return res.status(404).json({ result: 'not found maybe you not register' });
    
    const studentClass = await classes.find((r) => r.classID === find.class)
    await studentClass.courses.forEach((a) => {
        a.forEach((b, i) => {
            a[i] = b.courseCode;
        })
    })

    res.status(200).json({
        result: {
            name: find.username,
            family: find.family,
            idNumber: find.idNumber,
            phonenumber: find.phonenumber,
            day: getDay(),
            class: find.class,
            courses: studentClass.courses[getDay()],
        }
    });
});
App.get('/', async (req, res, next) => {
    var classes = [];

    await dbClasses.find({}).forEach((r) => {
        classes.push(r);
    });
    res.status(200).json({ result: classes });
})

App.get('/unregister', async (req, res, next) => {
    var students = [];

    await dbUnReg.find({ isRegister: false }).forEach((r) => {
        students.push(r);
    });
    res.status(200).json({ result: students });
})
module.exports = App;