# Details

Date : 2019-12-30 11:22:42

Directory h:\Nodejs\school\routes

Total : 12 files,  613 codes, 18 comments, 76 blanks, all 707 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [abPr.js](file:///h%3A/Nodejs/school/routes/abPr.js) | JavaScript | 54 | 0 | 6 | 60 |
| [addUsers.js](file:///h%3A/Nodejs/school/routes/addUsers.js) | JavaScript | 99 | 0 | 4 | 103 |
| [behavior.js](file:///h%3A/Nodejs/school/routes/behavior.js) | JavaScript | 27 | 0 | 6 | 33 |
| [classList.js](file:///h%3A/Nodejs/school/routes/classList.js) | JavaScript | 23 | 0 | 5 | 28 |
| [classes.js](file:///h%3A/Nodejs/school/routes/classes.js) | JavaScript | 57 | 0 | 5 | 62 |
| [course.js](file:///h%3A/Nodejs/school/routes/course.js) | JavaScript | 24 | 0 | 2 | 26 |
| [loginUser.js](file:///h%3A/Nodejs/school/routes/loginUser.js) | JavaScript | 22 | 0 | 3 | 25 |
| [personnel.js](file:///h%3A/Nodejs/school/routes/personnel.js) | JavaScript | 104 | 0 | 10 | 114 |
| [registerUser.js](file:///h%3A/Nodejs/school/routes/registerUser.js) | JavaScript | 31 | 0 | 7 | 38 |
| [score.js](file:///h%3A/Nodejs/school/routes/score.js) | JavaScript | 13 | 18 | 8 | 39 |
| [show.js](file:///h%3A/Nodejs/school/routes/show.js) | JavaScript | 93 | 0 | 11 | 104 |
| [student.js](file:///h%3A/Nodejs/school/routes/student.js) | JavaScript | 66 | 0 | 9 | 75 |

[summary](results.md)