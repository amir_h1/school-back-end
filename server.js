const Http = require('http');
const port = 8080;
const host = '0.0.0.0';
const App = require('./app');
require('./database');
const Server = Http.createServer(App);

Server.listen(port, host, () => console.log(`Server running on port: ${port}`));